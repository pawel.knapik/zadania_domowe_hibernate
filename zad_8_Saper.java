import java.util.concurrent.ThreadLocalRandom;

public class zad_8_Saper {

    public static void main(String[] args) {

        int n = ThreadLocalRandom.current().nextInt(3, 10);
        int m = ThreadLocalRandom.current().nextInt(3, 10);

        boolean[][]saperArray = saper(n,m);
        boolean[][]resultArray = saperArray.clone();
        printField(saperArray);
        printResult(result(resultArray,n,m));


    }
    private static boolean[][] saper(int n, int m) {
        boolean[][] array = new boolean[n][m];
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < m; j++) {
                    double p = ThreadLocalRandom.current().nextDouble();
                    if (p < 0.25 || p > 0.75) array[i][j] = true;
                    else array[i][j] = false;
                }
            }
        return array;
    }

    public static void printField(boolean[][] n) {
        System.out.println("Pole z zazanaczonymi bombami:");
        for (int i = 0; i < n.length; i++) {
            for (int j = 0; j < n[i].length; j++) {
                if(n[i][j]) System.out.print("|T|");
                else System.out.print("|N|");
            }
            System.out.println();
            for (int z = 0; z < n[i].length; z++) {System.out.print(" - ");}
            System.out.println();
        }
        System.out.println();
    }

    private static int[][] result(boolean[][] field,int n, int m) {
        int bomb = 99;
        int[][] array = new int[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if(field[i][j]) array[i][j]=bomb;
            }
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if(array[i][j]==bomb){
                        if(j+1<m && array[i][j+1]!=bomb) array[i][j+1]++;
                        if(j-1>=0 && array[i][j-1]!=bomb)  array[i][j-1]++;
                        if(i+1<n && array[i+1][j]!=bomb) array[i+1][j]++;
                        if(i-1>=0 && array[i-1][j]!=bomb) array[i-1][j]++;
                }
            }
        }
        return array;
    }

    public static void printResult(int[][] n) {
        System.out.println("Rozwiazanie sapera:");
        for (int i = 0; i < n.length; i++) {
            for (int j = 0; j < n[i].length; j++) {
                if(n[i][j]==99) System.out.print("|*|");
                else System.out.print("|" + n[i][j] + "|");
            }
            System.out.println();
            for (int z = 0; z < n[i].length; z++) {System.out.print(" - ");}
            System.out.println();
        }
        System.out.println();
    }
}
