import java.util.Scanner;

public class zad_11_AverageAcceleration {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter the starting velocity [m/s]");
        double v0 = scanner.nextDouble();
        System.out.println("Please enter the ending velocity [m/s]");
        double v1 = scanner.nextDouble();
        System.out.println("Please enter the time span [s]");
        int t = scanner.nextInt();
        System.out.println("Average acceleration is "+(v1-v0)/t);
    }
}
