public class zad_1_FlagOfUSA {

    public static void main(String[] args) {

        int stars = 9;
        int rows = 15;
        int columns = 50;

        doTheFlag(stars,rows,columns);
    }

    private static void doTheFlag(int a,int b, int c){

        String asterix = "*";
        String space = " ";
        String equals = "=";

        for (int i = 0; i <a ; i++) {
            if (i%2==0){
                System.out.println();
                for (int j = 0; j <c ; j++) {
                    if (j<a-3) System.out.print(asterix+space);
                    else System.out.print(equals);
                 }
            }else{
                System.out.println();
                System.out.print(space);
                for (int j = 0; j <c ; j++) {
                    if (j<a-4) System.out.print(asterix+space);
                    else System.out.print(equals);
                }
            }
        }
        System.out.println();
        for (int i = 0; i <b-a ; i++) {
            for (int j = 0; j <c+a-3 ; j++) {
                System.out.print(equals);
            }
            System.out.println();
        }
    }

}
