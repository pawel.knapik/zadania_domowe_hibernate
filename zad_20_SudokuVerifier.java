public class zad_20_SudokuVerifier {
    public static void main(String[] args) {

        printArray(generateSuodku());
        System.out.println(isItCorrect(generateSuodku()));

    }

    public static int[][] generateSuodku(){
        int [][]array = new int[9][9];

        int [] array1 = {5,3,4,6,7,8,9,1,2};
        int [] array2 = {6,7,2,1,9,5,3,4,8};
        int [] array3 = {1,9,8,3,4,2,5,6,7};
        int [] array4 = {8,5,9,7,6,1,4,2,3};
        int [] array5 = {4,2,6,8,5,3,7,9,1};
        int [] array6 = {7,1,3,9,2,4,8,5,6};
        int [] array7 = {9,6,1,5,3,7,2,8,4};
        int [] array8 = {2,8,7,4,1,9,6,3,5};
        int [] array9 = {3,4,5,2,8,6,1,7,9};

        for (int i = 0; i <9; i++) array[0][i]=array1[i];
        for (int i = 0; i <9; i++) array[1][i]=array2[i];
        for (int i = 0; i <9; i++) array[2][i]=array3[i];
        for (int i = 0; i <9; i++) array[3][i]=array4[i];
        for (int i = 0; i <9; i++) array[4][i]=array5[i];
        for (int i = 0; i <9; i++) array[5][i]=array6[i];
        for (int i = 0; i <9; i++) array[6][i]=array7[i];
        for (int i = 0; i <9; i++) array[7][i]=array8[i];
        for (int i = 0; i <9; i++) array[8][i]=array9[i];

        return array;
    }

    private static void printArray(int[][] n){
        for (int i = 0; i <n.length ; i++) {
            for (int j = 0; j <n[i].length ; j++) {
                System.out.print(n[i][j]);
            }
            System.out.println();
        }
    }

    private static String isItCorrect(int[][]a){
        String result = "This sudoku is correct";
        int correct = 1+2+3+4+5+6+7+8+9;

        for (int i = 0; i <a.length; i++) {
            int sum =0;
            for (int j = 0; j <a[i].length; j++) sum+=a[i][j];
            if (sum!=correct){
                result="This sudoku is not correct";
                return result;
            }
            sum=0;
        }
        for (int i = 0; i <a.length; i++) {
            int sum =0;
            for (int j = 0; j <a[i].length; j++) sum+=a[j][i];
            if (sum!=correct){
                result="This sudoku is not correct";
                return result;
            }
            sum=0;
        }
        return result;
    }
}


