import java.util.Scanner;

public class zad_13_DistanceBetweenTwoPoints {

    public static void main(String[] args) {
        System.out.println("Distance = " + calculateDistances());
    }

    public static double calculateDistances(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter first point coordinates in format x1,y1");
        String point1 = scanner.nextLine();
        System.out.println("Please enter second point coordinates in format x2,y2");
        String point2 = scanner.nextLine();

        String[]vector = point1.split(",");
        double x1 = Integer.parseInt(vector[0]);
        double y1 = Integer.parseInt(vector[1]);
        vector = point2.split(",");
        double x2 = Integer.parseInt(vector[0]);
        double y2 = Integer.parseInt(vector[1]);

        double distance = Math.sqrt(Math.pow((x2-x1),2) + Math.pow((y2-y1),2));

     return distance;
    }
}
