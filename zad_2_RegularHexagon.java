public class zad_2_RegularHexagon {

    public static void main(String[] args) {
        int a = 6;
        printAllFunctions(a);
    }

    public static void printAllFunctions(int a){
        System.out.println("Szesciokat foremny o dlugosci boku a = " + a);
        System.out.println();
        System.out.println("Pole : " + pole(a));
        System.out.println("Obwod : " + obwod(a));
        System.out.println("Dluzsza przekatna : " + dluzszaPrzekatna(a));
        System.out.println("Krotsza przekatna : " + krotszaPrzekatna(a));
        System.out.println("Promien okregu wpisanego : " + promienOkreguWpisanego(a));
    }

    public static double pole(int a){return (3*a*a*Math.sqrt(3))/2;}
    public static double obwod(int a){return 6*a;}
    public static double dluzszaPrzekatna(int a){return 2*a;}
    public static double krotszaPrzekatna(int a){return a*Math.sqrt(3);}
    public static double promienOkreguWpisanego(int a){return (a*Math.sqrt(3))/2;}
}
