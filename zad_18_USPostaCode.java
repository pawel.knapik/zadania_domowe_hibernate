public class zad_18_USPostaCode {
    public static void main(String[] args) {
        String code = "08540";
        print(conversion(code));
    }

    private static int[] conversion(String code){
        int [] array = new int[code.length()+1];
        for (int i = 0; i <array.length-1; i++) {
            array[i]=Character.getNumericValue(code.charAt(i));
        }
        int counter = 0;
        for (int i = 0; i <array.length-1 ; i++) {
            counter+=array[i];
        }
        counter = counter%10;
        array[array.length-1]=counter;
        return array;
    }

    private static void print(int []array){
        String longer = "*****";
        String  shorter = "**";

        System.out.println(longer);

        for (int i = 0; i <array.length ; i++) {
            switch (array[i]){
                case 0:
                    System.out.println(longer);
                    System.out.println(longer);
                    System.out.println(shorter);
                    System.out.println(shorter);
                    System.out.println(shorter);
                    break;
                case 1:
                    System.out.println(shorter);
                    System.out.println(shorter);
                    System.out.println(shorter);
                    System.out.println(longer);
                    System.out.println(longer);
                    break;
                case 2:
                    System.out.println(shorter);
                    System.out.println(shorter);
                    System.out.println(longer);
                    System.out.println(shorter);
                    System.out.println(longer);
                    break;
                case 3:
                    System.out.println(shorter);
                    System.out.println(shorter);
                    System.out.println(longer);
                    System.out.println(longer);
                    System.out.println(shorter);
                    break;
                case 4:
                    System.out.println(shorter);
                    System.out.println(longer);
                    System.out.println(shorter);
                    System.out.println(shorter);
                    System.out.println(longer);
                    break;
                case 5:
                    System.out.println(shorter);
                    System.out.println(longer);
                    System.out.println(shorter);
                    System.out.println(longer);
                    System.out.println(shorter);
                    break;
                case 6:
                    System.out.println(shorter);
                    System.out.println(longer);
                    System.out.println(longer);
                    System.out.println(shorter);
                    System.out.println(shorter);
                    break;
                case 7:
                    System.out.println(longer);
                    System.out.println(shorter);
                    System.out.println(shorter);
                    System.out.println(shorter);
                    System.out.println(longer);
                    break;
                case 8:
                    System.out.println(longer);
                    System.out.println(shorter);
                    System.out.println(shorter);
                    System.out.println(longer);
                    System.out.println(shorter);
                    break;
                case 9:
                    System.out.println(longer);
                    System.out.println(shorter);
                    System.out.println(longer);
                    System.out.println(shorter);
                    System.out.println(shorter);
                    break;
            }

        }
        System.out.println(longer);
    }

}
