public class zad_6_SecsOnFullHourFormat {

    public static void main(String[] args) {
        int sec = 86399;
        System.out.println(fullHour(sec));
    }

    public static String fullHour(int sec){
        int m = 60;
        int h = 3600;
        int hour = sec/h;
        sec = sec - h * hour;
        int min = sec/m;
        sec = sec - m * min;
        return Integer.toString(hour)+":"+Integer.toString(min)+":"+Integer.toString(sec);
    }
}
