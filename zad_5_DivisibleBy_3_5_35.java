public class zad_5_DivisibleBy_3_5_35 {

    public static void main(String[] args) {

        System.out.println("Liczby podzielne przez 3: ");
        for (int i = 1; i <101 ; i++) {
            if (i%3==0) System.out.print(i+", ");
        }
        System.out.println();
        System.out.println("Liczby podzielne przez 5: ");
        for (int i = 1; i <101 ; i++) {
            if (i%5==0) System.out.print(i+", ");
        }
        System.out.println();
        System.out.println("Liczby podzielne przez 3 i 5: ");
        for (int i = 1; i <101 ; i++) {
            if (i%3==0 & i%5==0) System.out.print(i+", ");
        }
    }
}
