public class zad_15_ChineseZodiac {

    public static void main(String[] args) {
        int year = 2019;
        System.out.println(chineseZodiac(year));

    }

    private static String chineseZodiac(int year){
        String result = null;
        int counter = year/12;
        counter= counter*12;
        counter = year-counter;

        switch (counter){
            case 0: result="monkey";
                break;
            case 1: result="rooster";
                break;
            case 2: result="dog";
                break;
            case 3: result="pig";
                break;
            case 4: result="rat";
                break;
            case 5: result="ox";
                break;
            case 6: result="tiger";
                break;
            case 7: result="rabbit";
                break;
            case 8: result="dragon";
                break;
            case 9: result="snake";
                break;
            case 10: result="horse";
                break;
            case 11: result="sheep";
                break;
        }
        return result;
    }
}
