import java.util.Scanner;

public class zad_12_EnergyNeeded {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter the amount of water in kilograms [kg]");
        double mass = scanner.nextDouble();
        System.out.println("Please enter initial temperature of water [C]");
        double temp0 = scanner.nextDouble();
        System.out.println("Please enter final temperature of water [C]");
        double temp1 = scanner.nextDouble();
        double shcOfWater= 4.180;
        double result = mass*(temp1-temp0)*shcOfWater;
        System.out.println("Energy needed to heat " + mass + "kg of water from " + temp0 + "C to " + temp1 +"C is " + result + " joules");
    }
}
