import java.util.Scanner;

public class zad_17_DayOfTheWeekFromDate {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please specify the year");
        int year = scanner.nextInt();
        System.out.println("Please specify the month");
        int month = scanner.nextInt();
        System.out.println("Please specify day of the month");
        int day = scanner.nextInt();

        System.out.println(dayOfWeek(year,month,day));

    }

    private static String dayOfWeek(int year,int month,int day){

        String result = null;
        int h, q, m, j, k;
        q =day;
        m=month;
        if(month==1){
            m=13;
            year-=1;
        } else if(month==2){
            m=14;
            year-=1;
        }
        j=year/100;
        k=year%100;

        h=(q+((26*(m+1))/10)+k+(k/4)+(j/4)+5*j)%7;

        switch (h){
            case 0: result="Saturday";
                break;
            case 1: result="Sunday";
                break;
            case 2: result="Monday";
                break;
            case 3: result="Tuesday";
                break;
            case 4: result="Wednesday";
                break;
            case 5: result="Thursday";
                break;
            case 6: result="Friday";
                break;
        }
        return result;
    }
}
