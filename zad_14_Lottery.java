import java.util.Arrays;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class zad_14_Lottery {

    public static void main(String[] args) {
        int firstNumber = ThreadLocalRandom.current().nextInt(0, 99);
        int secondNumber = ThreadLocalRandom.current().nextInt(0, 99);

        System.out.println(lotteryGenerator(firstNumber, secondNumber));

    }

    private static String lotteryGenerator(int first, int second) {
        String result = "Sorry you didn't win. Pleas try again!";
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please specify your first number lottery number from 0 to 99");
        int input1 = scanner.nextInt();
        System.out.println("Please specify your second number lottery number from 0 to 99");
        int input2 = scanner.nextInt();

        String firstLottery = Integer.toString(first);
        String secondLottery = Integer.toString(second);
        String firstInput = Integer.toString(input1);
        String secondInpupt = Integer.toString(input2);

        if (firstLottery.length() < 2) firstLottery = "0" + firstLottery;
        if (secondLottery.length() < 2) secondLottery = "0" + secondLottery;
        if (firstInput.length() < 2) firstInput = "0" + firstInput;
        if (secondInpupt.length() < 2) secondInpupt = "0" + secondInpupt;

        System.out.println("Your inputs: "+ firstInput + " " + secondInpupt);
        System.out.println("Lottery numbers: "+ firstLottery + " " + secondLottery);


        if (firstInput.equals(firstLottery) & secondInpupt.equals(secondLottery)) {
           result="Both number correct - your award is $10,000!";
           return result;
        }

        char combinedLottery[] = (firstLottery + secondLottery).toCharArray();
        char combinedInputs[] = (firstInput + secondInpupt).toCharArray();
        Arrays.sort(combinedLottery);
        Arrays.sort(combinedInputs);

        if (new String(combinedLottery).equals(new String(combinedInputs))){
           result ="All digits are the same - your award is $3,000";
           return result;
        }

        for (int i = 0; i < combinedInputs.length; i++) {
            for (int j = 0; j < combinedLottery.length; j++) {
                if (combinedInputs[i] == combinedLottery[j]) {
                    result = "At least one digit matches - your award is $1,000";
                    return result;
                }
            }
        }
        return result;
    }
}
