public class zad_3_CountSpaces {

    public static void main(String[] args) {

        String input = "abc  efae fqfqw fqfqe  ";
        System.out.println(countSpaces(input));

    }

    public static int countSpaces(String input){
        int counter = 0;
        char[] array = input.toCharArray();

        for (int i = 0; i <array.length ; i++) {
            if (array[i]==' ') counter++;
        }
        return counter;
    }
}
