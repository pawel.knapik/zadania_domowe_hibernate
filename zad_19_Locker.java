public class zad_19_Locker {

    public static void main(String[] args) {

        boolean[] array = new boolean[101];
        howManyLocked(array);
    }

    private static void howManyLocked(boolean [] array){
        int openLockers=0;
        for (int i = 1; i <array.length-1 ; i++) {
                int counter =2;
            for (int j = 1; j <array.length-1 ; j++) {
                if(j%counter==0){
                    if(array[j]) {
                        array[j]=false;
                    } else array[j]=true;

                }
            }
                counter++;
        }

        for (int i = 0; i <array.length-1 ; i++) {
            if (!array[i]) openLockers++;
        }
        openLockers--;
        System.out.println("There is "+openLockers+" open lockers");
    }
}

