public class zad_16_Spiraling2DArray {

    public static void main(String[] args) {
        int arrayLength = 5;
        printNormally(createArray(arrayLength));
        printSpiral(createArray(arrayLength));
    }
    private static int[][] createArray(int a){
        int counter = 1;

        int array[][] = new int[a][a];
        for (int i = 0; i <a ; i++) {
            for (int j = 0; j <a ; j++) {
                array[i][j]= counter;
                counter++;
            }
        }
        return array;
    }

    private static void printSpiral(int[][] array){
        for (int z = 0; z < array.length / 2; z++)
        {
            int min = z;
            int max = array.length - 1 - z;

            for (int j = min; j < max; j++) System.out.print(" " + array[min][j]);
            for (int i = min; i < max; i++) System.out.print(" " + array[i][max]);
            for (int j = max; j > min; j--) System.out.print(" " + array[max][j]);
            for (int i = max; i > min; i--) System.out.print(" " + array[i][min]);
        }
        if (array.length % 2 == 1) System.out.print(" " + array[array.length / 2][array.length / 2]);
        System.out.println();
    }

    private static void printNormally(int[][]a){
        for (int i = 0; i <a.length ; i++) {
            for (int j = 0; j <a[i].length ; j++) {
                System.out.print(String.format("%3d",a[i][j]));
            }
            System.out.println();
        }
        System.out.println();
    }
}



