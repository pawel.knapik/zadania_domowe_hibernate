import java.util.Arrays;
import java.util.StringJoiner;
import java.util.concurrent.ThreadLocalRandom;

public class zad_9_RandomWalker {

    public static void main(String[] args) {

        int n = 10;
        boolean[][] walkField = arrayGenerator(n);
        System.out.println(startWalk(walkField,startPoint(n)));

    }

    private static boolean[][] arrayGenerator (int n){
        boolean[][] array = new boolean[n][n];

        return  array;
    }

    private static int []startPoint(int n){
        int [] strPoint = new int[2];
        strPoint[0]= ThreadLocalRandom.current().nextInt(0, n-1);
        strPoint[1]= ThreadLocalRandom.current().nextInt(0, n-1);

        return strPoint;
    }

    private static boolean ifFalse(boolean[][] array){
        StringJoiner sj = new StringJoiner(System.lineSeparator());
        for (boolean[] row : array) {
            sj.add(Arrays.toString(row));
        }
        String result = sj.toString();
        if (result.contains("false")) return true;
        return false;
    }

    private static int startWalk(boolean[][] array,int[] point){
        int a = point[0];
        int b = point[1];
        int counter = 0;

        while (ifFalse(array)){
            double p = ThreadLocalRandom.current().nextDouble();
                if (p<0.25) {
                    if(b<array.length-1) {
                        b++;
                        array[a][b] = true;
                        counter++;
                    }
                } else if (p<0.50){
                    if(b>0) {
                        b--;
                        array[a][b] = true;
                        counter++;
                    }
                } else if (p<0.75){
                    if(a>0) {
                        a--;
                        array[a][b] = true;
                        counter++;
                    }
                } else if (p<1){
                    if(a<array.length -1) {
                        a++;
                        array[a][b] = true;
                        counter++;
                    }
                }
        }
//        printArray(array);
        return counter;
    }
    
//    private static void printArray(boolean[][] n){
//        for (int i = 0; i <n.length ; i++) {
//            for (int j = 0; j <n[i].length ; j++) {
//                System.out.print(n[i][j]);
//            }
//            System.out.println();
//        }
//    }
}
