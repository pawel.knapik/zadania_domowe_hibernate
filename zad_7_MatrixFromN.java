public class zad_7_MatrixFromN {

    public static void main(String[] args) {
        int n = 5;
        print(fillTheMatrix(n));
    }
    public static int[][] fillTheMatrix(int n){
        int counter = 1;
        int [][] array = new int[n][n];
        for (int i = 0; i <n ; i++) {
            for (int j = 0; j <n ; j++) {
                array[i][j]=j+counter;
                if (array[i][j]>n) array[i][j] = array[i][j] - n;
            }
            counter++;
        }
        return array;
    }
    public static void print(int[][] n){
        for (int i = 0; i < n.length; i++) {
            for (int j = 0; j < n[i].length; j++) {
                System.out.print(n[i][j] + " ");
            }
            System.out.println();
        }
    }
}
